export interface Sprint {
    name: String,
    duration: String,
    status: String,
    date: Date
}