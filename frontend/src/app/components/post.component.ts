import { Component } from '@angular/core';
import { ApiService } from '../api.service'
import {Sprint} from '../sprint.model'


@Component({
    selector: 'post',
    templateUrl: 'newSprint.component.html'
})  
export class PostComponent {
    constructor(private apiService: ApiService) { }
    postSprint: Sprint

    post(){
        this.apiService.postSprint(this.postSprint)        
    }
}
