import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { ApiService } from '../api.service'


@Component({
    selector: 'sprints',
    templateUrl: 'sprints.component.html'
})
export class SprintsComponent {

    constructor(private apiService: ApiService, private route: ActivatedRoute) { }

    ngOnInit() {
        var userId = this.route.snapshot.params.id

        this.apiService.getSprints(userId);
    }
}
