import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import {
  MatButtonModule,
  MatCardModule,
  MatToolbarModule,
  MatListModule
} from '@angular/material'
import { FormsModule } from '@angular/forms'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { MatTableModule } from '@angular/material'
import { CdkTableModule } from '@angular/cdk/table'

import { AppComponent } from './components/app.component'
import { ApiService } from './api.service'
import { AuthService } from './auth.service'
import { RegisterComponent } from './components/register.component'
import { LoginComponent } from './components/login.component'
import { MatInputModule } from '@angular/material/input'
import { UsersComponent } from './components/users.component'
import { ProfileComponent } from './components/profile.component'
import { PostComponent } from './components/post.component'
import { AuthInterceptorService } from './authInterceptor.service'
import { SprintsComponent } from './components/sprints.component'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatSelectModule } from '@angular/material/select';
import { HomeComponent } from './components/home/home/home.component'



const routes = [
  { path: 'newsprint', component: PostComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'users', component: UsersComponent },
  { path: 'profile/:id', component: ProfileComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    UsersComponent,
    ProfileComponent,
    PostComponent,
    SprintsComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    RouterModule.forRoot(routes),
    MatInputModule,
    BrowserAnimationsModule,
    MatListModule,
    MatTableModule,
    MatFormFieldModule,
    CdkTableModule,
    MatSelectModule
  ],
  providers: [ApiService, AuthService, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
