import{HttpClient}from '@angular/common/http'
import { Injectable } from '@angular/core'

@Injectable()
export class ApiService {    
    sprints =[]
    users=[]
    path='http://localhost:3000'

    constructor(private http: HttpClient) { }
    

    getSprints(userId) {
        this.http.get<any>(this.path + '/posts/' + userId).subscribe(res => {
            this.sprints = res
        })
    }

    postSprint(sprint) {
        this.http.post<any>(this.path + '/post', sprint).subscribe(res => {
            })
    }


    getUsers() {
        this.http.get<any>(this.path + '/users').subscribe(res => {
            this.users = res
        })
    }

    getProfile(id) {
        return this.http.get(this.path + '/profile/' + id)        
    }
}