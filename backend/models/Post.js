var mongoose = require('mongoose')


module.exports = mongoose.model('Post', {
    name: String,
    duration: String,
    status: String,
    author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
})